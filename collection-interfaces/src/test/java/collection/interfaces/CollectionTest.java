package collection.interfaces;

import java.util.ArrayList;

import junit.framework.TestCase;

public class CollectionTest extends TestCase {

    public void testInsert() {
        ArrayList<String> c = new ArrayList<String>();
        assertTrue(new InsertExercise().insertElem("elem", c).contains("elem"));
    }

}

